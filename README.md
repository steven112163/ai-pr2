# AI-PR2
Just for homework

***



## Prerequisite
* python >= 2.7.0 or 3.6.0

***



## Files
* cross200.txt: 200 rows of two-feature data with target of 1 or 2
* ellipse100.txt: 100 rows of two-feature data with target of 1 or 2
* glass.data: downloaded glass data, [glass](https://archive.ics.uci.edu/ml/datasets/glass+identification)
* ionosphere.data: downloaded ionosphere.data, [ionosphere](https://archive.ics.uci.edu/ml/datasets/ionosphere)
* iris.data: downloaded iris data, [iris](https://archive.ics.uci.edu/ml/datasets/Iris)
* wine.data: downloaded wine data, [wine](https://archive.ics.uci.edu/ml/datasets/Wine)
* Forest.py: a random forest which use tree bagging or attribute bagging
* Tree.py: a decision tree
* `__init__`.py: used to do some experiments
* PR2.pdf: description file

***



## Usage
```
$ python Forest.py
```



***



## Experiment Screen Shots

### Change ratio
![image of changing ratio](https://gitlab.com/steven112163/ai-pr2/blob/master/experiment%20screen%20shots/03%20iris%20ratio.png)

### Change number of trees
![image of changing number of trees](https://gitlab.com/steven112163/ai-pr2/blob/master/experiment%20screen%20shots/09%20iris%20numTree.png)

### Change relative ratio
![image of changing relative ratio](https://gitlab.com/steven112163/ai-pr2/blob/master/experiment%20screen%20shots/13%20iris%20relativeRatio.png)

### Change max depth
![image of changing max depth](https://gitlab.com/steven112163/ai-pr2/blob/master/experiment%20screen%20shots/19%20iris%20maxDepth.png)

### Change min size
![image of changing min size](https://gitlab.com/steven112163/ai-pr2/blob/master/experiment%20screen%20shots/25%20iris%20minSize.png)



***



## Reference
[How To Implement The Decision Tree Algorithm From Scratch In Python](https://machinelearningmastery.com/implement-decision-tree-algorithm-scratch-python/)
